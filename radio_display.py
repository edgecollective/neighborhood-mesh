import board
import displayio
import framebufferio
import sharpdisplay
import time
import digitalio
import digitalio       
        
# Release the existing display, if any
displayio.release_displays()

spi = board.SPI()
chip_select_pin = board.D5

# radio
cs = digitalio.DigitalInOut(board.D11)
reset = digitalio.DigitalInOut(board.D6)
import adafruit_rfm9x
rfm9x = adafruit_rfm9x.RFM9x(spi, cs, reset, 915.0)

# Select JUST ONE of the following lines:
# For the 400x240 display (can only be operated at 2MHz)
#framebuffer = sharpdisplay.SharpMemoryFramebuffer(bus, chip_select_pin, 400, 240)
# For the 144x168 display (can be operated at up to 8MHz)
framebuffer = sharpdisplay.SharpMemoryFramebuffer(spi, chip_select_pin, width=144, height=168, baudrate=8000000)

display = framebufferio.FramebufferDisplay(framebuffer)

from adafruit_display_text.label import Label
from terminalio import FONT
label = Label(font=FONT, text="hello", x=2, y=10, scale=2, line_spacing=1.2)
display.root_group = label

time.sleep(3)

index = 0

while True:

    rfm9x.send('Hello world!')
    print('send')
    label.text=str(index)
    index=index+1
    time.sleep(1)



