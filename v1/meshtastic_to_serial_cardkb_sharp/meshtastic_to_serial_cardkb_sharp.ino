/*
Example of processing incoming serial data without blocking.

Author:   Nick Gammon
Date:     13 November 2011. 
Modified: 31 August 2013.

Released for public use.
*/
#include <Adafruit_GFX.h>
#include <Adafruit_SharpMem.h>

// any pins can be used
#define SHARP_SCK  9
#define SHARP_MOSI 11
#define SHARP_SS   7

#define CARDKB_ADDR 0x5F  //Define the I2C address of CardKB.
#include <Wire.h>

// how much serial data we expect before a newline
const unsigned int MAX_INPUTTY = 50;

Adafruit_SharpMem display(SHARP_SCK, SHARP_MOSI, SHARP_SS, 144, 168);

#define BLACK 0
#define WHITE 1

int minorHalfSize; // 1/2 of lesser of display width or height

int dx = 0;
int dy = 0;

void setup ()
  {
      Wire.begin();
  Serial.begin (115200);
  Serial1.begin(38400);


  // Several shapes are drawn centered on the screen.  Calculate 1/2 of
  // lesser of display width or height, this is used repeatedly later.
  minorHalfSize = min(display.width(), display.height()) / 2;

  // start & clear the display
  display.begin();
  display.clearDisplay();
  display.setTextSize(1);
  display.setTextColor(BLACK);
  display.setCursor(0,0);
  display.cp437(true);
  display.write(">>");
  display.refresh();


  } // end of setup

// here to process incoming serial data after a terminator received
void process_input_data (const char * data)
  {
    /*
  // for now just display it
  // (but you could compare it to some value, convert to an integer, etc.)
  Serial.println(data);
  //display.write("<<");
  display.write(data);
  display.write("\n\n");
  display.refresh();
  */
//const char * out = &data[1];
//display.write(out);
Serial.print(data);
display.write(data);
//display.write("\n");
display.write("\n");
  display.refresh();



  }  // end of process_data

void process_output_data (const char * data)
  {
  // for now just display it
  // (but you could compare it to some value, convert to an integer, etc.)
  Serial1.println(data);
  display.write("\n");
  display.refresh();
  }  // end of process_data
  
void processIncomingByte (const byte inByte)
  {
  static char input_line [MAX_INPUTTY];
  static unsigned int input_pos = 0;

  switch (inByte)
    {

    case '\n':   // end of text
      input_line [input_pos] = 0;  // terminating null byte
      
      // terminator reached! process input_line here ...
      process_input_data (input_line);
      
      // reset buffer for next time
      input_pos = 0;  
      break;

    case '\r':   // discard carriage return
      break;

    default:
      // keep adding if not full ... allow for terminating null byte
      if (input_pos < (MAX_INPUTTY - 1))
        input_line [input_pos++] = inByte;
        //Serial.print(inByte);
      break;

    }  // end of switch
   
  } // end of processIncomingByte  



  //void processOutgoingByte (const byte outByte)
   void processOutgoingByte (char outByte)
  {
  static char output_line [MAX_INPUTTY];
  static unsigned int output_pos = 0;

  switch (outByte)
    {

    case 0:
      break;


    case '\x0D':   // end of text
      output_line [output_pos] = 0;  // terminating null byte
      Serial.println();
      display.write('\n');
      //dy=dy+5;
      //dx=0;    
      // terminator reached! process input_line here ...
      process_output_data (output_line);
      
      // reset buffer for next time
      output_pos = 0;  
      break;

    //case '\r':   // discard carriage return
    //break;

    default:
      // keep adding if not full ... allow for terminating null byte
      //Serial.print(outByte);
      if (output_pos < (MAX_INPUTTY - 1))
        output_line [output_pos++] = outByte;
        Serial.print(outByte);
        //display.setCursor(dx,dy);
        display.write(outByte);
        display.refresh();
        //dx=dx+1;
      break;

    }  // end of switch
   
  } // end of processIncomingByte  


void loop()
  {
  // if serial data available, process it
  while (Serial1.available () > 0)
    processIncomingByte (Serial1.read ());
  
  Wire.requestFrom(CARDKB_ADDR, 1); //Request 1 byte from the slave device.
   while (Wire.available())
    processOutgoingByte (Wire.read());
  
  // do other stuff here like testing digital input (button presses) ...

  }  // end of loop