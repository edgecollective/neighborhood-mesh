import board
import displayio
import framebufferio
import sharpdisplay
import time
        
# Release the existing display, if any
displayio.release_displays()

bus = board.SPI()
chip_select_pin = board.D5
# Select JUST ONE of the following lines:
# For the 400x240 display (can only be operated at 2MHz)
#framebuffer = sharpdisplay.SharpMemoryFramebuffer(bus, chip_select_pin, 400, 240)
# For the 144x168 display (can be operated at up to 8MHz)
framebuffer = sharpdisplay.SharpMemoryFramebuffer(bus, chip_select_pin, width=144, height=168, baudrate=8000000)

display = framebufferio.FramebufferDisplay(framebuffer)

from adafruit_display_text.label import Label
from terminalio import FONT
label = Label(font=FONT, text="BLACK\nLIVES\nMATTER", x=120, y=120, scale=4, line_spacing=1.2)
display.root_group = label
time.sleep(5)

