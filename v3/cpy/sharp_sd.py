import time
import board
import busio
import displayio
import framebufferio
import sharpdisplay
import busio
import digitalio
import adafruit_gps
import adafruit_sdcard
import storage

displayio.release_displays()

#spi = board.SPI()
spi = busio.SPI(board.D11, MOSI=board.D12)

chip_select_pin = board.D7

#framebuffer = sharpdisplay.SharpMemoryFramebuffer(bus, chip_select_pin, width=144, height=168, baudrate=8000000)
framebuffer = sharpdisplay.SharpMemoryFramebuffer(spi, chip_select_pin, width=400, height=240)

display = framebufferio.FramebufferDisplay(framebuffer)

from adafruit_display_text.label import Label
from terminalio import FONT

label1 = Label(font=FONT, text="> ", x=0, y=5, scale=1, line_spacing=1.2)
label2 = Label(font=FONT, text="(waiting...)", x=0, y=35, scale=1, line_spacing=1.2)
label3 = Label(font=FONT, text="-------messages-------", x=0, y=20, scale=1, line_spacing=1.2)
#display.root_group = label

text_group = displayio.Group()
text_group.append(label1)
text_group.append(label2)
text_group.append(label3)

display.root_group=text_group

i = 0

SD_CS = board.D10

# Connect to the card and mount the filesystem.
#spi = busio.SPI(board.SCK, board.MOSI, board.MISO)
spi = board.SPI()
cs = digitalio.DigitalInOut(SD_CS)
sdcard = adafruit_sdcard.SDCard(spi, cs)
vfs = storage.VfsFat(sdcard)
storage.mount(vfs, "/sd")

while True:

    label3.text=str(i)
    with open("/sd/temperature.txt", "a") as f:
        out_str = str(i)+", yep."
        f.write(out_str)
        print(i)
    i=i+1
    time.sleep(1)
