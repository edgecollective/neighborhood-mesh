# SPDX-FileCopyrightText: 2021 ladyada for Adafruit Industries
# SPDX-License-Identifier: MIT

"""
This test will initialize the display using displayio and draw a solid green
background, a smaller purple rectangle, and some yellow text.
"""
import board
import terminalio
import displayio
import busio
import digitalio

# Starting in CircuitPython 9.x fourwire will be a seperate internal library
# rather than a component of the displayio library
try:
    from fourwire import FourWire
except ImportError:
    from displayio import FourWire
from adafruit_display_text import label
from adafruit_st7789 import ST7789

# Release any resources currently in use for the displays
displayio.release_displays()

spi = board.SPI()
tft_cs = board.D6
tft_dc = board.D11

display_bus = FourWire(spi, command=tft_dc, chip_select=tft_cs, reset=board.D5)

display = ST7789(display_bus, width=240, height=240, rowstart=80,rotation=180)

# Make the display context
splash = displayio.Group()
display.root_group = splash

color_bitmap = displayio.Bitmap(240, 240, 1)
color_palette = displayio.Palette(1)
color_palette[0] = 0x00FF00  # Bright Green

bg_sprite = displayio.TileGrid(color_bitmap, pixel_shader=color_palette, x=0, y=0)
splash.append(bg_sprite)

# Draw a smaller inner rectangle
inner_bitmap = displayio.Bitmap(200, 200, 1)
inner_palette = displayio.Palette(1)
inner_palette[0] = 0xAA0088  # Purple
inner_sprite = displayio.TileGrid(inner_bitmap, pixel_shader=inner_palette, x=20, y=20)
splash.append(inner_sprite)

# Draw a label
#text_group = displayio.Group(scale=2, x=50, y=120)
text_group = displayio.Group()


outgoing_area = label.Label(terminalio.FONT, x=30,y=50,scale=2, text=">", color=0xFFFF00)

incoming_area = label.Label(terminalio.FONT, x=30,y=75,scale=2, text=">", color=0xFFFF00)




text_group.append(incoming_area)  # Subgroup for text scaling
text_group.append(outgoing_area)

splash.append(text_group)

uart = busio.UART(board.TX, board.RX, baudrate=38400)

uart_gps = busio.UART(board.A1, board.A2, baudrate=9600)

i2c = busio.I2C(board.SCL, board.SDA)

while not i2c.try_lock():
    pass
 
#cardkb = i2c.scan()[0]  # should return 95
#i2c_devices = i2c.scan()
#print(i2c_devices)
#cardkb = i2c.scan()[0]

ESC = chr(27)
NUL = '\x00'
CR = "\r"
LF = "\n"
TAB = bytearray(b'\x09')
LEFT = bytearray(b'\xB4')
RIGHT = bytearray(b'\xB7')
DOWN = bytearray(b'\xB6')
UP = bytearray(b'\xB5')
BACK = bytearray(b'\x08')
c = ''
b = bytearray(1)


instr = ''

while True:


    #i2c.readfrom_into(cardkb,b)
    #c=b.decode()
    b=''
    c=''
    if (b == LEFT):
        print("left!")
        continue
    if (b == RIGHT):
        print("right!")
        continue
    if (b == UP):
        print("up!")
        continue
        
    #c=b.decode()
    if (c == BACK) and len(instr)>0:
        print("DELETE")
        instr=instr[:-1]
        print("instr=",instr)
        outgoing_area.text='> '+instr
        
    if (c != ESC and c != NUL and c !=LEFT):
        if (c == CR):
            print('\nsending:',instr)
            outgoing_area.text='> (sending message ...)'
            outgoing_area.text='> '
            uart.write(bytes(instr, "ascii"))
            #send_message(sendee[1],instr.strip())
            #messages.append("me: "+instr.strip())
            #get_messages()
            instr=''
        else:
            print(c, end='')
            instr=instr+c
            outgoing_area.text='> '+instr
        
    gps_data = uart_gps.read(uart_gps.in_waiting)
    if gps_data is not None:
        g = gps_data.decode()
        if len(g) > 0:
            print(g)
        
    
    data = uart.read(uart.in_waiting)
    
    if data is not None:
        if len(data)>0:
        # convert bytearray to string
            data_string = ''.join([chr(b) for b in data])
            print(data_string, end="")
            incoming_area.text=data_string.strip()

