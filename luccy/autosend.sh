#!/bin/bash

for (( c=1; c<=5000; c++ ))
do 
   echo "Welcome $c times"
   meshtastic --port /dev/ttyUSB0 --ch-index 1 --sendtext "hello $c"
   sleep 30 
done
