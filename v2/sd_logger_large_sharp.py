# SPDX-FileCopyrightText: 2017 Limor Fried for Adafruit Industries
#
# SPDX-License-Identifier: MIT

import time

import adafruit_sdcard
import board
import busio
import digitalio
import microcontroller
import storage
import time
import board
import busio
import displayio
import framebufferio
import sharpdisplay
import busio
import digitalio
import adafruit_gps


displayio.release_displays()

spi = board.SPI()

display_spi = busio.SPI(board.D11, MOSI=board.D12)

#bus = board.SPI()
chip_select_pin = board.D7

framebuffer = sharpdisplay.SharpMemoryFramebuffer(display_spi, chip_select_pin, width=400, height=240)

display = framebufferio.FramebufferDisplay(framebuffer)

from adafruit_display_text.label import Label
from terminalio import FONT

label1 = Label(font=FONT, text="> ", x=0, y=5, scale=1, line_spacing=1.2)
label2 = Label(font=FONT, text="(waiting...)", x=0, y=35, scale=1, line_spacing=1.2)
label3 = Label(font=FONT, text="-------messages-------", x=0, y=20, scale=1, line_spacing=1.2)
#display.root_group = label

text_group = displayio.Group()
text_group.append(label1)
text_group.append(label2)
text_group.append(label3)

display.root_group=text_group
displayio.release_displays()


# Use any pin that is not taken by SPI
SD_CS = board.D10

led = digitalio.DigitalInOut(board.D13)
led.direction = digitalio.Direction.OUTPUT

# Connect to the card and mount the filesystem.
#sspi = busio.SPI(board.SCK, board.MOSI, board.MISO)

cs = digitalio.DigitalInOut(SD_CS)
sdcard = adafruit_sdcard.SDCard(spi, cs)
vfs = storage.VfsFat(sdcard)
storage.mount(vfs, "/sd")

# Use the filesystem as normal! Our files are under /sd

print("Logging temperature to filesystem")
# append to the file!
i=0
while True:
    # open file for append
    with open("/sd/temperature.txt", "a") as f:
        led.value = True  # turn on LED to indicate we're writing to the file
        t = microcontroller.cpu.temperature
        out_str = str(i)+", yep."
        print(out_str)
        #print("Temperature = %0.1f" % t)
        #f.write("%0.1f\n" % t)
        f.write(out_str)
        label1.text = "> writing: "+str(i)+"\n"
        display.refresh()
        led.value = False  # turn off LED to indicate we're done
        i=i+1
    # file is saved
    time.sleep(5)

