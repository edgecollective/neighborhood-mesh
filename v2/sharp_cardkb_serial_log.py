import time
import board
import busio
import displayio
import framebufferio
import sharpdisplay
import busio
import digitalio


messages = []

displayio.release_displays()

bus = board.SPI()
chip_select_pin = board.D7

framebuffer = sharpdisplay.SharpMemoryFramebuffer(bus, chip_select_pin, width=144, height=168, baudrate=8000000)

display = framebufferio.FramebufferDisplay(framebuffer)

from adafruit_display_text.label import Label
from terminalio import FONT

label1 = Label(font=FONT, text="READY", x=0, y=5, scale=1, line_spacing=1.2)
label2 = Label(font=FONT, text="READY", x=0, y=30, scale=1, line_spacing=1.2)
#display.root_group = label

text_group = displayio.Group()
text_group.append(label1)
text_group.append(label2)

display.root_group=text_group
#display.show(text_group)

i2c = busio.I2C(board.SCL, board.SDA)

while not i2c.try_lock():
    pass
 
cardkb = i2c.scan()[0]  # should return 95
if cardkb != 95:
    print("!!! Check I2C config: " + str(i2c))
    print("!!! CardKB not found. I2C device", cardkb,
          "found instead.")
    exit(1)
 
ESC = chr(27)
NUL = '\x00'
CR = "\r"
LF = "\n"
LEFT = bytearray(b'\xB4')
RIGHT = bytearray(b'\xB7')
DOWN = bytearray(b'\xB6')
UP = bytearray(b'\xB5')
c = ''
b = bytearray(1)

instr = ''
radio_instr = ''
uart = busio.UART(board.TX, board.RX, baudrate=38400,timeout=0)

msg_display_index=0
while True:

    i2c.readfrom_into(cardkb,b)
    if (b == LEFT):
        print("left!")
        continue
    if (b == RIGHT):
        print("right!")
        continue
    if (b == UP):
        print("up!")
        if(len(messages)>0):
            msg_display_index=(msg_display_index-1)%len(messages)
            print("index=",msg_display_index)
            label2.text="<"+str(msg_display_index)+"> "+messages[msg_display_index]
        continue
    if (b == DOWN):
        print("down!")
        if(len(messages)>0):
            msg_display_index=(msg_display_index+1)%len(messages)
            print("index=",msg_display_index)
            label2.text="<"+str(msg_display_index)+"> "+messages[msg_display_index]
        continue
    
    c=b.decode()
    if (c != ESC and c != NUL and c !=LEFT):
        if (c == CR):
            print('\nsending:',instr)
            label1.text='READY'
            uart.write(bytes(instr, "ascii"))
            instr=''
        else:
            print(c, end='')
            instr=instr+c
            label1.text=instr
            
    data = uart.read(1)  # read up to 32 bytes
    if data is not None:
        this=''.join([chr(b) for b in data])
        if this == LF or this == CR:
            if this == CR:
                continue
            else:
                #print(data,this)
                print(radio_instr)
                if (len(radio_instr)>0):
                    messages.append(radio_instr.strip())
                    print('messages:',messages)
                    msg_display_index=len(messages)-1
                    label2.text="<"+str(msg_display_index)+"> "+messages[msg_display_index]
                radio_instr=''
                #radio_instr=radio_instr+'\n'
        else:
            radio_instr=radio_instr+this
 
# be nice, clean up
i2c.unlock()
