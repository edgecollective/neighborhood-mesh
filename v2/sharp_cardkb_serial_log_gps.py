import time
import board
import busio
import displayio
import framebufferio
import sharpdisplay
import busio
import digitalio
import adafruit_gps

ESC = chr(27)
NUL = '\x00'
CR = "\r"
LF = "\n"

messages = []

displayio.release_displays()

bus = board.SPI()
chip_select_pin = board.D5

framebuffer = sharpdisplay.SharpMemoryFramebuffer(bus, chip_select_pin, width=144, height=168, baudrate=8000000)

display = framebufferio.FramebufferDisplay(framebuffer)

from adafruit_display_text.label import Label
from terminalio import FONT

label1 = Label(font=FONT, text="> ", x=0, y=5, scale=1, line_spacing=1.2)
label2 = Label(font=FONT, text="(waiting...)", x=0, y=35, scale=1, line_spacing=1.2)
label3 = Label(font=FONT, text="-------messages-------", x=0, y=20, scale=1, line_spacing=1.2)
#display.root_group = label

text_group = displayio.Group()
text_group.append(label1)
text_group.append(label2)
text_group.append(label3)

display.root_group=text_group
#display.show(text_group)

#i2c = busio.I2C(board.SCL, board.SDA)

i2c = board.I2C()

gps = adafruit_gps.GPS_GtopI2C(i2c, debug=False)  # Use I2C interface

gps.send_command(b"PMTK314,0,1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0")
gps.send_command(b"PMTK220,1000")


c = ''
b = bytearray(1)

instr = ''
radio_instr = ''
uart = busio.UART(board.TX, board.RX, baudrate=38400,timeout=0)

msg_display_index=0

message_lines = 9

def show_messages(highlight):
    # highlight is the last message
    end_message=highlight
    start_message = (highlight-message_lines)%len(messages)
    print("highlight:",highlight)
    print("bounds:",start_message,end_message)
    outstr=''
    i = end_message
    count=0
    while (count < message_lines) and (count < len(messages)):
        outstr="<"+str(i)+"> "+messages[i]+'\n'+outstr
        i=(i-1)%len(messages)
        count=count+1
    label2.text=outstr

last_print = time.monotonic()

while True:

    
    current = time.monotonic()
    
    if current - last_print >= 10.0:
        last_print = current
        gps.update()
        if not gps.has_fix:
            # Try again if we don't have a fix yet.
            print("Waiting for fix...")
            label1.text = '> '+'(no gps fix)'
            continue
        lat = "{0:.6f}".format(gps.latitude)
        lon = "{0:.6f}".format(gps.longitude)
        out_str = lat+","+lon
        label1.text = '> '+out_str
        uart.write(bytes(out_str, "ascii"))
        #messages.append("me: "+instr.strip())
        #msg_display_index=len(messages)-1
        #show_messages(msg_display_index)
        
    data = uart.read(1)  # read up to 32 bytes
    if data is not None:
        this=''.join([chr(b) for b in data])
        if this == LF or this == CR:
            if this == CR:
                continue
            else:
                #print(data,this)
                print(radio_instr)
                if (len(radio_instr)>0):
                    messages.append(radio_instr.strip())
                    print('messages:',messages)
                    msg_display_index=len(messages)-1
                    #label2.text="<"+str(msg_display_index)+"> "+messages[msg_display_index]
                    show_messages(msg_display_index)
                radio_instr=''
                #radio_instr=radio_instr+'\n'
        else:
            radio_instr=radio_instr+this
    
    
        
 
# be nice, clean up

