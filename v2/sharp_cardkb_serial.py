import time
import board
import busio
import displayio
import framebufferio
import sharpdisplay
import busio
import digitalio



displayio.release_displays()

bus = board.SPI()
chip_select_pin = board.D7

framebuffer = sharpdisplay.SharpMemoryFramebuffer(bus, chip_select_pin, width=144, height=168, baudrate=8000000)

display = framebufferio.FramebufferDisplay(framebuffer)

from adafruit_display_text.label import Label
from terminalio import FONT

label1 = Label(font=FONT, text="READY", x=0, y=5, scale=1, line_spacing=1.2)
label2 = Label(font=FONT, text="READY", x=0, y=30, scale=1, line_spacing=1.2)
#display.root_group = label

text_group = displayio.Group()
text_group.append(label1)
text_group.append(label2)

display.root_group=text_group
#display.show(text_group)

i2c = busio.I2C(board.SCL, board.SDA)

while not i2c.try_lock():
    pass
 
cardkb = i2c.scan()[0]  # should return 95
if cardkb != 95:
    print("!!! Check I2C config: " + str(i2c))
    print("!!! CardKB not found. I2C device", cardkb,
          "found instead.")
    exit(1)
 
ESC = chr(27)
NUL = '\x00'
CR = "\r"
LF = "\n"
c = ''
b = bytearray(1)

instr = ''
radio_instr = ''
uart = busio.UART(board.TX, board.RX, baudrate=38400,timeout=0)

while True:

    i2c.readfrom_into(cardkb,b)
    c=b.decode()
    if (c != ESC and c != NUL):
        if (c == CR):
            print('\nsending:',instr)
            label1.text='sending:'+instr
            uart.write(bytes(instr, "ascii"))
            instr=''
        else:
            print(c, end='')
            instr=instr+c
            label1.text=instr
            
    data = uart.read(1)  # read up to 32 bytes
    if data is not None:
        #data_string = ''.join([chr(b) for b in data])
        this=''.join([chr(b) for b in data])
        if this == CR:
            continue
        if this == LF:
            #print(data,this)
            label2.text=str(radio_instr).strip()
            print(radio_instr)
            radio_instr=''
            #print("ending!")
        else:
            #this=''.join([chr(b) for b in data])
            #print(data,this)
            radio_instr=radio_instr+this
            
            #print(radio_instr)
            #radio_instr = radio_instr.join([chr(b) for b in data])
            #print(radio_instr)
            #label2.text=str(radio_instr)
            #print(radio_instr, end="")
            #radio_instr = radio_instr+chr(b)
            #data_string = ''.join([chr(b) for b in data])
            #print(data_string, end="")
            #label.text=data_string.strip()
        
    #time.sleep(0.005)
 
# be nice, clean up
i2c.unlock()
