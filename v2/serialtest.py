# SPDX-FileCopyrightText: 2018 Kattni Rembor for Adafruit Industries
#
# SPDX-License-Identifier: MIT

"""CircuitPython Essentials UART Serial example"""

import board
import displayio
import framebufferio
import sharpdisplay
        
# Release the existing display, if any
displayio.release_displays()


import board
import busio
import digitalio


bus = board.SPI()
chip_select_pin = board.D7
# Select JUST ONE of the following lines:
# For the 400x240 display (can only be operated at 2MHz)
#framebuffer = sharpdisplay.SharpMemoryFramebuffer(bus, chip_select_pin, 400, 240)
# For the 144x168 display (can be operated at up to 8MHz)
framebuffer = sharpdisplay.SharpMemoryFramebuffer(bus, chip_select_pin, width=144, height=168, baudrate=8000000)

display = framebufferio.FramebufferDisplay(framebuffer)


uart = busio.UART(board.TX, board.RX, baudrate=38400)

from adafruit_display_text.label import Label
from terminalio import FONT
label = Label(font=FONT, text="READY", x=0, y=5, scale=1, line_spacing=1.2)
display.root_group = label

while True:
    data = uart.read(32)  # read up to 32 bytes
    # print(data)  # this is a bytearray type

    if data is not None:

        # convert bytearray to string
        data_string = ''.join([chr(b) for b in data])
        print(data_string, end="")
        label.text=data_string.strip()


