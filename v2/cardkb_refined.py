import time
import board
import busio
 
i2c = busio.I2C(board.SCL, board.SDA)
 
while not i2c.try_lock():
    pass
 
cardkb = i2c.scan()[0]  # should return 95
if cardkb != 95:
    print("!!! Check I2C config: " + str(i2c))
    print("!!! CardKB not found. I2C device", cardkb,
          "found instead.")
    exit(1)
 
ESC = chr(27)
NUL = '\x00'
CR = "\r"
LF = "\n"
c = ''
b = bytearray(1)

instr = ''

while True:

    i2c.readfrom_into(cardkb,b)
    c=b.decode()
    if (c != ESC and c != NUL):
        if (c == CR):
            print('\nsending:',instr)
            instr=''
        else:
            print(c, end='')
            instr=instr+c
        
    time.sleep(0.005)
 
# be nice, clean up
i2c.unlock()
