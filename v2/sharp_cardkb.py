import time
import board
import busio
import displayio
import framebufferio
import sharpdisplay
import busio
import digitalio



displayio.release_displays()

bus = board.SPI()
chip_select_pin = board.D7

framebuffer = sharpdisplay.SharpMemoryFramebuffer(bus, chip_select_pin, width=144, height=168, baudrate=8000000)

display = framebufferio.FramebufferDisplay(framebuffer)

from adafruit_display_text.label import Label
from terminalio import FONT

label = Label(font=FONT, text="READY", x=0, y=5, scale=1, line_spacing=1.2)
display.root_group = label

i2c = busio.I2C(board.SCL, board.SDA)

while not i2c.try_lock():
    pass
 
cardkb = i2c.scan()[0]  # should return 95
if cardkb != 95:
    print("!!! Check I2C config: " + str(i2c))
    print("!!! CardKB not found. I2C device", cardkb,
          "found instead.")
    exit(1)
 
ESC = chr(27)
NUL = '\x00'
CR = "\r"
LF = "\n"
c = ''
b = bytearray(1)

instr = ''

while True:

    i2c.readfrom_into(cardkb,b)
    c=b.decode()
    if (c != ESC and c != NUL):
        if (c == CR):
            print('\nsending:',instr)
            label.text='sending:'+instr
            instr=''
        else:
            print(c, end='')
            instr=instr+c
            label.text=instr
        
    time.sleep(0.005)
 
# be nice, clean up
i2c.unlock()
