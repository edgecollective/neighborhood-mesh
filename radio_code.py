import board
import busio
import digitalio
import time
spi = busio.SPI(board.SCK, MOSI=board.MOSI, MISO=board.MISO)

cs = digitalio.DigitalInOut(board.D11)
reset = digitalio.DigitalInOut(board.D6)

import adafruit_rfm9x
rfm9x = adafruit_rfm9x.RFM9x(spi, cs, reset, 915.0)

while True:

    rfm9x.send('Hello world!')
    print('send')
    time.sleep(1)
